#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2021/10/28 下午 01:56
# @Author : Aries
# @Site : 
# @File : main.py
# @Software: PyCharm

import requests
from bs4 import BeautifulSoup
import json

def get_data(start_date, end_date):
    headers = {
    'accept': 'text/plain, */*; q=0.01',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'content-length': '191',
    'content-type': 'application/x-www-form-urlencoded',
    'cookie': 'PHPSESSID=hlmj1qikebdupfdh28n9190grk; SideBlockUser=a%3A2%3A%7Bs%3A10%3A%22stack_size%22%3Ba%3A1%3A%7Bs%3A11%3A%22last_quotes%22%3Bi%3A8%3B%7Ds%3A6%3A%22stacks%22%3Ba%3A1%3A%7Bs%3A11%3A%22last_quotes%22%3Ba%3A1%3A%7Bi%3A0%3Ba%3A3%3A%7Bs%3A7%3A%22pair_ID%22%3Bs%3A4%3A%226408%22%3Bs%3A10%3A%22pair_title%22%3Bs%3A0%3A%22%22%3Bs%3A9%3A%22pair_link%22%3Bs%3A28%3A%22%2Fequities%2Fapple-computer-inc%22%3B%7D%7D%7D%7D; geoC=TW; adBlockerNewUserDomains=1635399041; StickySession=id.26563637206.286cn.investing.com; udid=6079085e42d635d37d898b6f42e3861d; smd=6079085e42d635d37d898b6f42e3861d-1635399041; __cflb=0H28uxmf5JNxjDUC6WDvQUEoJyvKUTrtLdNZRHnzsQG; protectedMedia=2; __gads=ID=88597ace9b78d82e:T=1635399042:S=ALNI_MbdjLyQRU3NAd42t5XN0ZSiN8LxKg; _ga=GA1.2.1931883151.1635399043; _gid=GA1.2.1190097492.1635399044; Hm_lvt_a1e3d50107c2a0e021d734fe76f85914=1635399046; G_ENABLED_IDPS=google; adsFreeSalePopUp=3; logglytrackingsession=201d4495-d93f-4b93-b66a-09756618a038; outbrain_cid_fetch=true; __cf_bm=bNED2OFKzoXZikRrV5.9mwonmu3cfqlhZa62tlUAvw0-1635403314-0-AbzrosGTfdTLm8VK2i81tlaL+HrY5iWZJk8c/pKLMle3xgqmbqWBpuHmKj/c20XCJ92W6TKPV1WNLde6SocWxKlwEf+Innb+uaaohw/GJSQ/VrDN7mMBqsuGqbsceeJWQw==; nyxDorf=ZWFjMmI0M3E1Yjo2NGYzLzVsPntiZDY3MzY%3D; Hm_lpvt_a1e3d50107c2a0e021d734fe76f85914=1635403332',
    'origin': 'https://cn.investing.com',
    'referer': 'https://cn.investing.com/equities/apple-computer-inc-historical-data',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest'
    }
    body = {'curr_id': '6408','smlID':'1159963','header':'AAPL历史数据','st_date':f'{start_date}','end_date':f'{end_date}','interval_sec':'Daily','sort_col':'date','sort_ord':'DESC','action':'historical_data'}
    r = requests.post('https://cn.investing.com/instruments/HistoricalDataAjax', headers=headers,data=body)

    return r.text

def process(html_text):
    soup = BeautifulSoup(html_text, 'html.parser')
    all_value = soup.find_all('td',class_=['redFont','greenFont'])

    data = []
    for value_index in range(0, len(all_value)):
        if value_index % 2 == 0:
            data.append(all_value[value_index].text)

    json_arr = json.dumps(data)

    return json_arr



if __name__ == "__main__":
    text = get_data(start_date='2021/09/28', end_date='2021/10/28')
    arr = process(text)
    print(arr)